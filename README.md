# Encryption-files-SourceGuardian

This repository contains decrypted files with original content related to *watchvideo* and *key* API. The files in this repository will be encrypted (obfuscated) in the product.


### What files does this repo have? ###

* videoDRMController.php
* videoValidation.php
* domain.php


